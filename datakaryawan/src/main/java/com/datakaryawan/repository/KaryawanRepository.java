package com.datakaryawan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datakaryawan.model.Karyawan;

public interface KaryawanRepository extends JpaRepository<Karyawan, Long> {
  //List<Karyawan> findByPublished(long salary);

  List<Karyawan> findByNamaContaining(String nama);
}