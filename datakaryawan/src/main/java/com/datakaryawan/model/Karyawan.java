package com.datakaryawan.model;

import javax.persistence.*;

@Entity
@Table(name = "tblkaryawan")
public class Karyawan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "nama")
	private String nama;

	@Column(name = "departemen")
	private String departemen;

	@Column(name = "salary")
	private long salary;

	@Column(name = "test")
	private String test;

	public Karyawan() {

	}

	public Karyawan(String nama, String departemen, long salary, String test) {
		this.nama = nama;
		this.departemen = departemen;
		this.salary = salary;
		this.test = test;
	}

	public long getId() {
		return id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getDepartemen() {
		return departemen;
	}

	public void setDepartemen(String departemen) {
		this.departemen = departemen;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}
	
	public String gettest() {
		return test;
	}

	public void settest(String test) {
		this.test = test;
	}

	@Override
	public String toString() {
		return "Karyawan [id=" + id + ", nama=" + nama + ", departemen=" + departemen + ", salary=" + salary + ", test = " + test +"]";
	}
}