package com.datakaryawan.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datakaryawan.model.Karyawan;
import com.datakaryawan.repository.KaryawanRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class KaryawanController {

	@Autowired
	KaryawanRepository karyawanRepository;

	@GetMapping("/karyawans")
	public ResponseEntity<List<Karyawan>> getAllKaryawans(@RequestParam(required = false) String nama) {
		try {
			List<Karyawan> karyawans = new ArrayList<Karyawan>();

			if (nama == null)
				karyawanRepository.findAll().forEach(karyawans::add);
			else
				karyawanRepository.findByNamaContaining(nama).forEach(karyawans::add);

			if (karyawans.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(karyawans, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/karyawans/{id}")
	public ResponseEntity<Karyawan> getKaryawanById(@PathVariable("id") long id) {
		Optional<Karyawan> tutorialData = karyawanRepository.findById(id);

		if (tutorialData.isPresent()) {
			return new ResponseEntity<>(tutorialData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/karyawans")
	public ResponseEntity<Karyawan> createKaryawan(@RequestBody Karyawan karyawan) {
		try {
			Karyawan _karyawan = karyawanRepository
					.save(new Karyawan(karyawan.getNama(), karyawan.getDepartemen(), karyawan.getSalary(), karyawan.gettest() ));
			return new ResponseEntity<>(_karyawan, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/karyawans/{id}")
	public ResponseEntity<Karyawan> updateKaryawan(@PathVariable("id") long id, @RequestBody Karyawan karyawan) {
		Optional<Karyawan> tutorialData = karyawanRepository.findById(id);

		if (tutorialData.isPresent()) {
			Karyawan _tutorial = tutorialData.get();
			_tutorial.setNama(karyawan.getNama());
			_tutorial.setDepartemen(karyawan.getDepartemen());
			_tutorial.setSalary(karyawan.getSalary());
			return new ResponseEntity<>(karyawanRepository.save(_tutorial), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/karyawans/{id}")
	public ResponseEntity<HttpStatus> deleteKaryawan(@PathVariable("id") long id) {
		try {
			karyawanRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/karyawans")
	public ResponseEntity<HttpStatus> deleteAllKaryawans() {
		try {
			karyawanRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	//@GetMapping("/karyawans/salary")
	//public ResponseEntity<List<Karyawan>> findBySalary() {
	//	try {
	//		List<Karyawan> karyawans = karyawanRepository.findByPublished(true);
        
	//		if (tutorials.isEmpty()) {
	//			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	//		}
	//		return new ResponseEntity<>(tutorials, HttpStatus.OK);
	//	} catch (Exception e) {
	//		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	//	}
	//}
}